package tasks.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;


class TasksOperationsTest {

    private TasksOperations tasksOperations;
    private ObservableList<Task> taskObservableList;


    @BeforeEach
    void setUp() {
        this.taskObservableList = fillObservableList();
        this.tasksOperations =  new TasksOperations(taskObservableList);
    }

    @AfterEach
    void tearDown() {
        this.taskObservableList =null;
        this.tasksOperations = null;
    }

    private ObservableList<Task> fillObservableList(){
        ObservableList<Task> observableList;
        Date first = new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime();
        Date second = new GregorianCalendar(2017, Calendar.FEBRUARY, 11).getTime();
        Task task = new Task("First task", first,second,1);
        Date first1 = new GregorianCalendar(2017, Calendar.FEBRUARY, 6).getTime();
        Date second1 = new GregorianCalendar(2021, Calendar.MARCH, 9).getTime();

        Task task2 = new Task("Second task",first1,second1,1);


        observableList = FXCollections.observableArrayList(Arrays.asList(task,task2));
        return observableList;
    }

    @Test
    void filterBetweenData() {
        System.out.println(this.taskObservableList);
        Date first = new GregorianCalendar(2013, Calendar.FEBRUARY, 7).getTime();
        Date second = new GregorianCalendar(2015, Calendar.APRIL, 12).getTime();
        Iterable<Task> tasks = this.tasksOperations.incoming(first,second);
        assert(tasks.iterator().next().getTitle().equals("First task"));
    }

    @Test
    void invalidDate() {
        Date first = new GregorianCalendar(2012, Calendar.FEBRUARY, 7).getTime();
        Date second = new GregorianCalendar(2012, Calendar.APRIL, 12).getTime();
        Iterable<Task> tasks = this.tasksOperations.incoming(first,second);
        assert(!tasks.iterator().hasNext());
    }


    @Test
    void invalidFirstDate() {
        Date first = new GregorianCalendar(2013, Calendar.FEBRUARY, 7).getTime();
        Date second = new GregorianCalendar(2015, Calendar.APRIL, 12).getTime();
        Iterable<Task> tasks = this.tasksOperations.incoming(first,second);
        assert(tasks.iterator().next().getTitle().equals("First task"));
    }


    @Test
    void emptyList() {
        TasksOperations tasksOperations = new TasksOperations(FXCollections.observableArrayList(new ArrayList<>()));
        Iterable<Task> tasks = tasksOperations.incoming(null,null);
        assert(!tasks.iterator().hasNext());
    }


}