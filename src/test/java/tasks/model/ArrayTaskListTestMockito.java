package tasks.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class ArrayTaskListTestMockito {

    @Mock
    private ArrayTaskList arrayTaskList;


    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testAdd(){
        Date date = new GregorianCalendar(2013, Calendar.FEBRUARY, 7).getTime();
        Task task = new Task("Task",date);
        Mockito.doNothing().when(arrayTaskList).add(task);
        this.arrayTaskList.add(task);
        Mockito.verify(arrayTaskList,Mockito.times(1)).add(task);
        assertEquals(arrayTaskList.getAll().size(),0);
    }

    @Test
    void testGetAll(){
        Date date = new GregorianCalendar(2013, Calendar.FEBRUARY, 7).getTime();
        Task task = new Task("Task",date);
        Mockito.when(arrayTaskList.getAll()).thenReturn(Arrays.asList(task));

        assertEquals(this.arrayTaskList.getAll().get(0).getTitle(),"Task");
        assertEquals(this.arrayTaskList.getAll().size(),1);

        Mockito.verify(arrayTaskList,Mockito.times(2)).getAll();
    }
}