package tasks.model;

import org.junit.jupiter.api.*;
import tasks.controller.NewEditController;
import tasks.services.TasksService;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ArrayTaskListTest {

    private ArrayTaskList arrayTaskList;
    private NewEditController newEditController;
    private TasksService tasksService;

    @BeforeEach
    void setUp() {
        arrayTaskList = new ArrayTaskList();
        this.tasksService = new TasksService(this.arrayTaskList);
        this.newEditController = new NewEditController();
        this.newEditController.setService(this.tasksService);
    }

    @AfterEach
    void tearDown() {
        arrayTaskList = null;
        tasksService = null;
        newEditController = null;
    }

    //-------------------ECP

    @Deprecated
    @Test
    void addNullTask() {
        try{
            arrayTaskList.add(null);
        }catch (NullPointerException ex){
            assert (ex.getMessage().equals("Task shouldn't be null"));
        }
    }

    @Test
    @Timeout(1000)
    @DisplayName("Date invalid")
    void addTaskDateInvalid() {
        try {
//            Task task = new Task("Task 1", new Integer(2));
//            this.arrayTaskList.add(task);
        } catch (IllegalArgumentException ex) {
            assert false;
        }
    }
    @Test
    @Timeout(1000)
    @DisplayName("Date valid")
    void addTaskDateValid() {
        try {
            Task task = new Task("Task 1", new Date());
            this.arrayTaskList.add(task);
            assert(this.arrayTaskList.getAll().size() == 1);
            List<Task> tasks = this.arrayTaskList.getAll().stream().filter(x->x.getTitle().equals("Task 1")).collect(Collectors.toList());
            assert(tasks.get(0).getTitle().equals("Task 1"));
        } catch (Exception ex) {
            assert false;
        }
    }



    @Test
    @Disabled
    @DisplayName("Title integer")
    void addTaskTitleInteger() {
        try {
            Date d = new Date();
//            Task task = new Task(2, d);
//            this.arrayTaskList.add(task);
        } catch (Exception ex) {
            assert false;
        }
    }

    @Test
    @RepeatedTest(3)
    @DisplayName("Title string")
        void addTaskTitleString() {
        try {
            Date d = new Date();
            Task task = new Task("Null", d);
            this.arrayTaskList.add(task);
            List<Task> tasks = this.arrayTaskList.getAll().stream().filter(x->x.getTitle().equals("Null")).collect(Collectors.toList());
            assert(tasks.size() != 0);
            assert(tasks.get(0).getTitle().equals("Null"));
        } catch (Exception ex) {
            assert false;
        }
    }

    @Test
    @RepeatedTest(3)
    @DisplayName("Title string ''")
        void addTaskTitleStringNull() {
        try {
            Date d = new Date();
            Task task = new Task("", d);
            this.arrayTaskList.add(task);
            List<Task> tasks = this.arrayTaskList.getAll().stream().filter(x->x.getTitle().equals("")).collect(Collectors.toList());
            assert(tasks.size() != 0);
            assert(tasks.get(0).getTitle().equals(""));
        } catch (Exception ex) {
            assert false;
        }
    }

    //-------------------BVA

    @Test
    @DisplayName("Time less than 0")
    void addTaskTimeLessThanZero() {
        try {
            Date d = new Date();
            d.setTime(-1);
            Task task = new Task("Null", d);
            this.arrayTaskList.add(task);
        } catch (Exception ex) {
            assert (ex.getMessage().equals("Time cannot be negative"));
        }
    }

    @Test
    @DisplayName("Time = 0")
    void addTaskTimeEqualWithZero() {
        try {
            Date d = new Date();
            d.setTime(0);
            Task task = new Task("Null", d);
            this.arrayTaskList.add(task);
            List<Task> tasks = this.arrayTaskList.getAll().stream().filter(x->x.getTitle().equals("Null")).collect(Collectors.toList());
            assert(tasks.size() != 0);
            assert(tasks.get(0).getTime().toString().equals("Thu Jan 01 02:00:00 EET 1970"));
        } catch (Exception ex) {
            assert (ex.getMessage().equals("Time cannot be negative"));
        }
    }

    @Test
    @DisplayName("Time grater than 0")
    void addTaskTimeGraterThanZero() {
        try {
            Date d = new Date();
            d.setTime(1);
            Task task = new Task("Null", d);
            this.arrayTaskList.add(task);
            List<Task> tasks = this.arrayTaskList.getAll().stream().filter(x->x.getTitle().equals("Null")).collect(Collectors.toList());
            assert(tasks.size() != 0);
        } catch (Exception ex) {
            assert false;
        }
    }

    @Test
    @DisplayName("Interval less than 1")
    void addTaskIntervalLessThanOne() {
        try {
            Date s = new Date();
            Date e = new Date();
            Task task = new Task("Null", s,e,0);
            this.arrayTaskList.add(task);
        } catch (Exception ex) {
            assert (ex.getMessage().equals("interval should me > 1"));
        }
    }

    @Test
    @DisplayName("Interval = 1")
    void addTaskIntervalEqualWithOne() {
        try {
            Date s = new Date();
            Date e = new Date();
            Task task = new Task("Null", s,e,1);
            this.arrayTaskList.add(task);
        } catch (Exception ex) {
            assert (ex.getMessage().equals("interval should me > 1"));
        }
    }

    @Test
    @DisplayName("Interval grater than 1")
    void addTaskIntervalGraterThanOne() {
        try {
            Date s = new Date();
            Date e = new Date();
            Task task = new Task("Null", s,e,22);
            this.arrayTaskList.add(task);
            List<Task> tasks = this.arrayTaskList.getAll();
            for(Task item: tasks){
                if(item.getRepeatInterval() == 0){
                    assert(item.getStartTime() != null && item.getEndTime() != null);
                }
            }
        } catch (Exception ex) {
            assert false;
        }
    }

}