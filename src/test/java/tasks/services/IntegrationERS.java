package tasks.services;

import javafx.collections.FXCollections;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import tasks.model.ArrayTaskList;
import tasks.model.Task;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;


class IntegrationERS {

    private ArrayTaskList arrayTaskList;

    private TasksService tasksService;

    @BeforeEach
    void setUp() {
        this.arrayTaskList = new ArrayTaskList();
        this.tasksService = new TasksService(this.arrayTaskList);
    }

    @AfterEach
    void tearDown() {
    }



    @Test
    void testCompleteGetAll(){
        Date date = new GregorianCalendar(2013, Calendar.FEBRUARY, 7).getTime();
        Task task = new Task("Task",date);
        this.tasksService.addTask(task);
        assertEquals(this.tasksService.getObservableList().get(0).getTitle(),"Task");
        assertEquals(this.tasksService.getObservableList().get(0).getTime(),date);
    }

    @Test
    void testCompleteAdd(){
        Date date = new GregorianCalendar(2013, Calendar.FEBRUARY, 7).getTime();
        Task task = new Task("Task",date);
        this.arrayTaskList.add(task);
        boolean status = arrayTaskList.getAll().size() == 1;
        assert  status == true;
    }
}