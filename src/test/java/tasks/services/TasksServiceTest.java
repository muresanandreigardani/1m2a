package tasks.services;

import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import tasks.model.ArrayTaskList;
import tasks.model.Task;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

class TasksServiceTest {

    @Mock
    private ArrayTaskList arrayTaskList;

    @InjectMocks
    private TasksService tasksService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testFilter(){
        Date date = new GregorianCalendar(2013, Calendar.FEBRUARY, 7).getTime();
        Task task = new Task("Task",date);
        Date anotherDate = new GregorianCalendar(2005, Calendar.FEBRUARY, 7).getTime();
        Date testDate = new GregorianCalendar(2014, Calendar.FEBRUARY, 7).getTime();
        Task anotherTask = new Task("Task",anotherDate);
        this.arrayTaskList.add(task);
        this.arrayTaskList.add(anotherTask);
        Mockito.doReturn(Arrays.asList(task)).when(arrayTaskList).getAll();
        ArrayList<Task> taskList = (ArrayList<Task>) this.tasksService.filterTasks(date,testDate);
        assertEquals(taskList.get(0),task);
    }

    @Test
    void testGetObservable(){
        Date date = new GregorianCalendar(2013, Calendar.FEBRUARY, 7).getTime();
        Task task = new Task("Task",date);
        Date date2 = new GregorianCalendar(2016, Calendar.FEBRUARY, 7).getTime();
        Task task2 = new Task("Task2",date2);
        this.arrayTaskList.add(task);
        this.arrayTaskList.add(task2);
        Mockito.doReturn(Arrays.asList(task)).when(arrayTaskList).getAll();
        ObservableList<Task> observableList = this.tasksService.getObservableList();
        Mockito.verify(arrayTaskList, times(1)).getAll();
        assertEquals(observableList.get(0),task);
        assertEquals(observableList.size(),1);
    }
}