package tasks.services;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import tasks.model.ArrayTaskList;
import tasks.model.Task;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

class IntegrationRS {

    @Mock
    private ArrayTaskList arrayTaskList;

    @InjectMocks
    private TasksService tasksService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addTask(){
        Date date = new GregorianCalendar(2013, Calendar.FEBRUARY, 7).getTime();
        Task task = new Task("Task",date);
        Mockito.when(arrayTaskList.getAll()).thenReturn(Arrays.asList(task));
        assertEquals(this.tasksService.getObservableList().size(),1);
        Mockito.verify(arrayTaskList, times(1)).getAll();
    }

    @Test
    void deleteTask(){
        Date date = new GregorianCalendar(2013, Calendar.FEBRUARY, 7).getTime();
        Task task = new Task("Task",date);
        Mockito.doNothing().when(arrayTaskList).add(task);
        this.arrayTaskList.add(task);
        Mockito.verify(arrayTaskList, times(1)).add(task);
        Mockito.when(arrayTaskList.remove(task)).thenReturn(true);
        boolean ok = this.tasksService.deleteTask(task);
        Mockito.verify(arrayTaskList, times(1)).remove(task);
        assertTrue (ok);
    }


}